# mdbook-labels
A preprocessor for [mdBook](https://github.com/rust-lang/mdBook) to render GitLab-like labels in HTML.

This renders labels like `~foo` and `~"foo bar"` similar to labels are rendered in GitLab.

## Usage
- clone this repo
- compile (e.g. `cargo build --release`)
- copy compiled binary to `$PATH`
- add to your book.toml: `[preprocessor.labels]`
