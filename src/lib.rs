#[macro_use]
extern crate lazy_static;

use clap::crate_name;
use mdbook::book::{Book, BookItem};
use mdbook::errors::Error;
use mdbook::preprocess::{Preprocessor, PreprocessorContext};
use regex::Regex;

lazy_static! {
    static ref LABEL_RE: Regex = Regex::new("~(?P<label>\".+\"|\\S+)").unwrap();
}

static STYLE: &str = r#"padding: 10px;
background-color: lightgrey;
border-radius: 20px;"#;

pub struct LabelProcessor;

impl LabelProcessor {
    pub fn new() -> LabelProcessor {
        LabelProcessor
    }
}

impl Preprocessor for LabelProcessor {
    fn name(&self) -> &str {
        crate_name!()
    }

    fn run(&self, _ctx: &PreprocessorContext, mut book: Book) -> Result<Book, Error> {
        book.for_each_mut(replace_labels);
        Ok(book)
    }

    fn supports_renderer(&self, renderer: &str) -> bool {
        renderer == "html"
    }
}

fn replace_labels(book_item: &mut BookItem) {
    match book_item {
        BookItem::Chapter(c) => {
            c.content = LABEL_RE
                .replace_all(
                    &c.content,
                    format!("<span style=\"{}\">~$label</span>", STYLE).as_str(),
                )
                .to_string();
        }
        BookItem::Separator => {}
    }
}
